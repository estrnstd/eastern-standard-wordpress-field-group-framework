<?php
/*
Plugin Name: Eastern Standard WordPress Field Group Framework
Plugin URI: https://bitbucket.org/estrnstd/eastern-standard-wordpress-field-group-framework/src/v2
Description: A system for configuring custom field groups.
Version: 2.2.0
Author: Eastern Standard
Author URI: https://easternstandard.com
Update URI: eswp-field-group-framework
*/

require_once(ABSPATH . 'wp-admin/includes/plugin.php');

$eswp_dependency_manager_path = ABSPATH . 'wp-content/plugins/eswp-dependency-manager/eswp-dependency-manager.php';
if (file_exists($eswp_dependency_manager_path)) {
	include_once $eswp_dependency_manager_path;
}

if (!class_exists('ESWP_Field_Group_Framework')) {
	#[AllowDynamicProperties]
	class ESWP_Field_Group_Framework {

		public static $default_field_key_separator = '__';

		public static function normalize_fields($fields = [], $parent_key = '') {
			if (is_array($fields)) {
				foreach ($fields as &$field) {
					if (!array_key_exists('key', $field)) {
						$field['key'] = $parent_key . self::$default_field_key_separator . $field['name'];
					}
					if (array_key_exists('sub_fields', $field) && is_array($field['sub_fields'])) {
						$field['sub_fields'] = self::normalize_fields($field['sub_fields'], $field['key']);
					}
					if (array_key_exists('layouts', $field) && is_array($field['layouts'])) {
						foreach ($field['layouts'] as $key => &$layout) {
							if (!array_key_exists('key', $layout)) {
								$layout['key'] = $key;
							}
							if (array_key_exists('sub_fields', $layout) && is_array($layout['sub_fields'])) {
								$layout['sub_fields'] = self::normalize_fields($layout['sub_fields'], $layout['key']);
							}
						}
					}
				}
			}
			return $fields;
		}

		public static function normalize_field_group($field_group, $path, $default = []) {
			if (is_array($default)) {
				foreach ($default as $key => $value) {
					if (!array_key_exists($key, $field_group)) {
						$field_group[$key] = $default[$key];
					}
				}
			}
			if (!array_key_exists('key', $field_group) || !is_string($field_group['key'])) {
				trigger_error(__("Field group 'key' in {$path} field group must be a string."), E_USER_WARNING);
			}
			if (!array_key_exists('fields', $field_group) || !is_array($field_group['fields'])) {
				trigger_error(__("Field group 'fields' in {$path} field group must be an array."), E_USER_WARNING);
			}
			if (!array_key_exists('title', $field_group) && is_string($field_group['key'])) {
				$field_group['title'] = $field_group['key'];
			}
			if (is_string($field_group['key']) && is_array($field_group['fields'])) {
				$field_group['fields'] = self::normalize_fields($field_group['fields'], $field_group['key']);
			}
			return $field_group;
		}

		public static function normalize_field_group_directories($field_group_directories) {
			$input_valid = true;
			$normalized_field_group_directories = [];

			if (is_string($field_group_directories) || is_array($field_group_directories)) {
				if (is_string($field_group_directories)) {
					$field_group_directories = [$field_group_directories];
				}
				if (is_array($field_group_directories)) {
					foreach ($field_group_directories as $field_group_directory) {
						if (!is_string($field_group_directory)) {
							$input_valid = false;
							break;
						}
					}
				}
			}
			else {
				$input_valid = false;
			}

			if (!$input_valid) {
				trigger_error(__("Class 'ESWP_Field_Group_Framework', method 'normalize_field_group_directories', parameter: '\$field_group_directories' must be a string or an array of strings."), E_USER_WARNING);
				return $normalized_field_group_directories;
			}

			// Handle globs
			foreach ($field_group_directories as $key => $field_group_directory) {
				if (str_contains($field_group_directory, '*')) {
					array_splice($field_group_directories, $key, 1, glob($field_group_directory));
				}
			}

			// Remove trailing slashes
			foreach ($field_group_directories as &$field_group_directory) {
				$field_group_directory = rtrim($field_group_directory, '/');
			}

			$normalized_block_directories = $field_group_directories;
			return $normalized_block_directories;
		}

		/**
		 * initialize_field_group
		 * 
		 * Adds missing values where possible and registers a field group.
		 * 
		 * @param   string  $field_group_directory  Required. A path to a field group directory.
		 * @return  void
		 */

		public static function initialize_field_group($field_group_directory) {
			$field_config_path = $field_group_directory . '/config.php';
			$field_group = include $field_config_path;
			$field_group = self::normalize_field_group($field_group, $field_config_path);
			acf_add_local_field_group($field_group);
		}

		/**
		 * initialize_field_groups
		 * 
		 * Adds missing values where possible and registers field groups.
		 * 
		 * @param   mixed  $field_group_directories  Required. A string or array of strings representing paths to field group directories.
		 * @return  void
		 */

		public static function initialize_field_groups($field_group_directories) {
			$field_group_directories = self::normalize_field_group_directories($field_group_directories);
			foreach ($field_group_directories as $field_group_directory) {
				self::initialize_field_group($field_group_directory);
			}
		}

		public function __construct() {

			$this->file = __FILE__;
			$this->dir = __DIR__;
			$this->plugin_data = get_plugin_data(__FILE__);
			$this->slug = plugin_basename(__DIR__);
			$this->basename_path = $this->slug . '/' . $this->slug . '.php';
			$this->dashboard_updating = true;
			$this->info_url = 'https://bitbucket.org/estrnstd/eastern-standard-wordpress-field-group-framework/raw/v2/info.json';
			$this->info_cache_allowed = true;
			$this->info_cache_key = $this->slug . '-info';
			$this->info_cache_length = DAY_IN_SECONDS;
			$this->plugin_dependencies = [
				[
					'name' => 'Advanced Custom Fields Pro', // Optional. Readable name of the plugin.
					'url' => 'https://www.advancedcustomfields.com/pro', // Optional. Link to the plugin.
					'path' => 'advanced-custom-fields-pro/acf.php', // Required. Path to the plugin relative to the plugins directory.
					'version' => '>=5.8', // Required. Semver constraints.
				],
			];

			if (class_exists('ESWP_Updater')) {
				ESWP_Updater::initialize_plugin_updates($this);
			}
			else {
				add_action('plugin-eswp-updater:initialized', function() {
					ESWP_Updater::initialize_plugin_updates($this);
				});
			}

			if (class_exists('ESWP_Dependency_Manager')) {
				$valid_dependencies = ESWP_Dependency_Manager::check_plugin_dependencies($this);
			}

		}

	}

	new ESWP_Field_Group_Framework();

}
