<?php

// https://www.advancedcustomfields.com/resources/register-fields-via-php

// Required
return [
	'key' => 'example_field_group',
	'title' => 'Example Field Group',
	'fields' => [
		[
			'label' => 'Example Text Field',
			'name' => 'example_text_field',
			'type' => 'text',
			'instructions' => 'Example text field instructions.',
			'required' => 1,
			'conditional_logic' => 0,
			'default_value' => 'Example text field default value.',
			'placeholder' => 'Example text field placeholder',
		],
	],
	'location' => [
		[
			[
				'param' => 'block',
				'operator' => '==',
				'value' => 'acf/example-block',
			],
		],
	],
	'menu_order' => 0,
	'position' => 'normal',
	'style' => 'default',
	'label_placement' => 'top',
	'instruction_placement' => 'label',
	'hide_on_screen' => '',
	'active' => true,
	'description' => '',
	'show_in_rest' => 0,
];
